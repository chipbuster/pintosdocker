#!/bin/bash

curl -O 'https://www.cs.utexas.edu/~ans/classes/cs439/projects/pintos/pintos.tar.gz'
tar xf pintos.tar.gz
mv src pintos
rm pintos.tar.gz
