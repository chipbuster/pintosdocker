Pintos Docker Container
=======================

This is a docker build meant for the CS439 course at UT Austin. The structure
of the directories/files/programs mirrors the lab systems in GDC as closely as
possible.

Unfortunately, it is impossible to exactly emulate the lab systems using any
technology, so you should *always* verify your results on the lab systems before
submission. However, this container may be useful if you want to work on your
own machine, or can't come into GDC because of a hypothetical global pandemic,
or are traveling and don't have a stable network connection, but still want to
work on Pintos.

## Build Instructions

`docker build -t pintos .`

## Usage Instructions

TODO
