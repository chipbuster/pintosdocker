FROM ubuntu:18.04

RUN apt-get update
RUN apt-get upgrade --assume-yes
RUN apt-get install --assume-yes \
# Core requrirements
curl vim build-essential sed qemu \
# Debugging requirements
rr gdb \
# Needed for multilib builds of gcc
gcc-multilib g++-multilib \
# Needed for Pintos perl scripts
perl-base perl-modules \
# Needed for bochs
libncurses5-dev \
# Needed for qemu
zlib1g-dev libglib2.0-dev python

RUN mkdir -p /opt/bin

######################
## GCC INSTALLATION ##
######################

# Needed for multilib build of gcc: https://askubuntu.com/a/252095
ENV LIBRARY_PATH /usr/lib/x86_64-linux-gnu

WORKDIR /root
RUN curl -O 'https://ftp.gnu.org/gnu/gcc/gcc-4.2.4/gcc-4.2.4.tar.bz2'
RUN tar xf gcc-4.2.4.tar.bz2

# Apply patch file to linux-unwind for modern libc: https://stackoverflow.com/a/48458759
WORKDIR /root/gcc-4.2.4/gcc/config/i386/
COPY patches/gcc/linux-unwind.patch .
RUN patch linux-unwind.h linux-unwind.patch

# Prepare a nice little place for all our stuff to live
RUN mkdir -p /lusr/opt/gcc-4.2.4

RUN mkdir /root/gcc-4.2.4/build
WORKDIR /root/gcc-4.2.4/build
RUN ../configure --enable-languages=c,c++ --prefix=/lusr/opt/gcc-4.2.4

# Change the compilers to use the right f89 flags: https://stackoverflow.com/a/35230665
# RHEL bug report: https://bugzilla.redhat.com/show_bug.cgi?id=476370
RUN sed -i 's/CC = gcc/CC = gcc -fgnu89-inline/' Makefile
RUN sed -i 's/CXX = c++/CXX = g++ -fgnu89-inline/' Makefile

# Build and install the compiler
RUN make -j$(nproc)
RUN make install

########################
## BOCHS INSTALLATION ##
########################

# Download and extract bochs source + patches
WORKDIR /root
COPY patches/bochs/ /root/bochs-patches/
RUN curl -L -o bochs-2.2.6.tar.gz 'https://sourceforge.net/projects/bochs/files/bochs/2.2.6/bochs-2.2.6.tar.gz/download'
RUN tar xf bochs-2.2.6.tar.gz
WORKDIR /root/bochs-2.2.6/

# Apply patches to bochs
RUN cat /root/bochs-patches/bochs-2.2.6-ms-extensions.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-big-endian.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-jitter.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-triple-fault.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-solaris-tty.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-page-fault-segv.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-paranoia.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-gdbstub-ENN.patch | patch -p1
RUN cat /root/bochs-patches/bochs-2.2.6-namespace.patch | patch -p1
# Guaranteed not to be on SunOS (this is an ubuntu docker!) so don't apply last patch

RUN mkdir /lusr/opt/bochs-2.2.6-pintos/

# Build Bochs with the GDBStub
WORKDIR /root/bochs-2.2.6/gdbstub-build
RUN ../configure --with-term --with-nogui --prefix=/lusr/opt/bochs-2.2.6-pintos/\
    --enable-cpu-level=6 --enable-gdb-stub
RUN make -j$(nproc)
RUN make install

# Build Bochs with the internal debugger
WORKDIR /root/bochs-2.2.6/debugger-build
RUN ../configure --with-term --with-nogui --prefix=/lusr/opt/bochs-2.2.6-pintos/\
    --enable-cpu-level=6 --enable-debugger
RUN make
RUN cp bxcommit /lusr/opt/bochs-2.2.6-pintos/bin/bxcommit-dbg
RUN cp bximage /lusr/opt/bochs-2.2.6-pintos/bin/bximage-dbg

#################################
## PINTOS UTILITY INSTALLATION ##
#################################
RUN mkdir /lusr/opt/pintos

COPY pintosutil/ /root/pintosutil/
WORKDIR /root/pintosutil
RUN make -j4

RUN cp backtrace pintos pintos-gdb pintos-set-cmdline pintos-mkdisk Pintos.pm\
       setitimer-helper squish-unix squish-pty /lusr/opt/pintos/

############################
## GDBSCRIPT INSTALLATION ##
############################

COPY patches/bochs/gdb-macros /lusr/opt/pintos/gdb-macros

################
## QEMU SETUP ##
################

# Turns out I don't need QEMU setup. Whoops.

#WORKDIR /root
#RUN curl -O "https://download.qemu.org/qemu-0.15.0.tar.xz"
#RUN tar xf qemu-0.15.0.tar.xz
#
## Patch QEMU against new glibc
#WORKDIR /root/qemu-0.15.0
#COPY patches/qemu/signal.patch .
#COPY patches/qemu/user-exec.patch .
#COPY patches/qemu/Makefile.target.patch .
#RUN patch -p0 < signal.patch 
#RUN patch -p0 < user-exec.patch
#RUN patch -p0 < Makefile.target.patch
#
## Build qemu
#WORKDIR /root/qemu-0.15.0/build
#RUN mkdir /lusr/opt/qemu-0.15.0
#RUN ../configure --prefix=/lusr/opt/qemu-0.15.0
#RUN make -j$(nproc)
#RUN make install

#################
## FINAL SETUP ##
#################

COPY fetch_pintos_skel.sh /usr/local/bin/fetchpintos

# PATH modifications so we can actually find the programs we just set up
RUN echo "PATH=\"/lusr/opt/bochs-2.2.6-pintos/bin:\
/lusr/opt/gcc-4.2.4/bin:\
/lusr/opt/qemu-0.15.0/bin:\
/lusr/opt/pintos:\$PATH\"" >> /etc/profile

WORKDIR /pintos
RUN chmod 777 /pintos
CMD ["bash","-l"]
